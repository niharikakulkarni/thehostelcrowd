import React from 'react';
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import Notifications from './Components/Notifications';
import Chat from './Components/Chat';
import Empty from './EmptyStacks';
import {Ionicons } from '@expo/vector-icons';

const navigator = createStackNavigator({
        Home: { screen: Notifications },
        Chat: { screen: Chat },
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'black',
            }
        },
    });

const EmptyStackOne = createStackNavigator(
    {
        Links: Empty,
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'black',
            }
        },
    }
);

const EmptyStackTwo = createStackNavigator(
    {
        Links: Empty,
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'black',
            }
        },
    }
);

const EmptyStackThree = createStackNavigator(
    {
        Links: Empty,
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: 'black',
            }
        },
    }
);

navigator.navigationOptions = {
    tabBarOptions: {
        showLabel : false,
    },
    tabBarIcon: <Ionicons name={'ios-chatboxes'} size={35}/>,
};

EmptyStackOne.navigationOptions = {
    tabBarOptions: {
        showLabel : false,
    },
    tabBarIcon: <Ionicons name={'ios-send'} size={35}/>,
};

EmptyStackTwo.navigationOptions = {
    tabBarOptions: {
        showLabel : false,
    },
    tabBarIcon: <Ionicons name={'ios-exit'} size={35}/>,
};

EmptyStackThree.navigationOptions = {
    tabBarOptions: {
        showLabel : false,
    },
    tabBarIcon: <Ionicons name={'ios-speedometer'} size={35} />,
};


const tabNavigator = createBottomTabNavigator({
    navigator,
    EmptyStackOne,
    EmptyStackTwo,
    EmptyStackThree
});

export default createAppContainer(
    createSwitchNavigator({
        Main: tabNavigator,
    })
);