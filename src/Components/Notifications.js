import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import ChatBar from './ChatBar';
import NewNotification from './NewNotification';
import PreviousNotifications from './PreviousNotifications';
import {loadNotifications, loadUsers, loadMessages} from '../state/Actions';
import users from '../../data/users';
import {notifications} from '../../data/notifications';
import messages from '../../data/messages';
import store from '../state/store';

class Notifications extends React.Component {
    constructor(props) {
        super(props);
        this.state = store.getState();
        store.subscribe(()=>{
            this.setState(store.getState());
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        const {users, notifications} = this.state.loaders;
        return (
            <ScrollView style={styles.container}>
                <ChatBar navigate={navigate} users={users}/>
                <NewNotification/>
                <PreviousNotifications users={users} notifications={notifications}/>
            </ScrollView>
        );
    }
}

const mapStateToProps = state => ({
    users: state.loaders.users,
    notifications: state.loaders.notifications,
    messages: state.loaders.messages,
});

const mapDispatchToProps = dispatch => ({
    getUsers: dispatch(loadUsers(users)),
    getNotifications: dispatch(loadNotifications(notifications)),
    getMessages: dispatch(loadMessages(messages)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
});
