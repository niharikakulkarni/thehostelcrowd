import React from 'react';
import {StyleSheet, Image, Text, TouchableOpacity, ScrollView, View} from 'react-native';

export default class ChatBar extends React.Component {
    constructor(props){
        super(props);
        this.openConversation.bind(this);
    }

    openConversation = (user) => {
        this.props.navigate('Chat', {user: user})
    };

    render() {
        const {users} = this.props;
        const icons = Object.keys(users).map(user =>
            <TouchableOpacity activeOpacity={0.9} key={users[user].id} onPress={() => this.openConversation(user)}>
                <Image source={{uri: users[user].picture}} style={styles.image}/>
            </TouchableOpacity>);

        return (
            <ScrollView
                style={styles.container}
                contentContainerStyle={{ alignItems: 'center' }}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                <View style={styles.newChat}>
                    <Text style={styles.newText}>new chat</Text>
                </View>
                {icons}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'row',
        paddingHorizontal: '5%',
     },
    image: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginHorizontal: 7,
    },
    newChat: {
        height: 60,
        width: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    newText: {
        textAlign: 'center',
        fontSize: 13,
    }
});