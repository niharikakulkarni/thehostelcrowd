import React from 'react';
import {StyleSheet, Text, View, FlatList, Image} from 'react-native';

export default class PreviousNotifications extends React.Component {
    render() {
        const {users, notifications} = this.props;
        return (
            <View style={styles.container}>
                <Text style={{color:'white', paddingLeft: '5%',}}>Previous Notifications</Text>
                <FlatList
                    style={styles.list}
                    data={notifications}
                    keyExtractor= {(item) => item.user}
                    renderItem={
                        ({item}) => <View key={item.id} style={styles.item}>
                            <Image source={{uri: users[item.user].picture}} style={styles.image} />
                            <View>
                                <Text style={styles.name}>{`${users[item.user].name} ${users[item.user].lastname}`}</Text>
                                <Text style={styles.type}>{item.type}</Text>
                                <Text style={styles.preview}>{item.preview}</Text>
                                <Text style={styles.byline}>BHAI Chat</Text>
                            </View>
                        </View>
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        marginTop: 15,
    },
    list: {
        flex:1,
        padding: '5%',
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: 15,
    },
    item: {
        flexDirection: 'row',
        marginBottom: 10,
        alignItems: 'flex-start',
    },
    name: {
        color: 'white',
        fontSize: 11,
        fontWeight: '300',
    },
    type: {
        color: 'white',
        fontWeight: '500',
    },
    preview: {
        color: 'white',
        fontSize: 11,
        fontStyle: 'italic',
        fontWeight: '300',
    },
    byline: {
        color: 'white',
        fontWeight: '500',
    }
});
