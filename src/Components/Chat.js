import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, FlatList } from 'react-native';
import Bubble from './Bubble';
import { Entypo, Ionicons } from '@expo/vector-icons';
import store from '../state/store';

export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = store.getState();
        store.subscribe(()=>{
            this.setState(store.getState());
        });
    }

    render() {
        const {users, messages} = this.state.loaders;
        const user = this.props.navigation.getParam('user', 'New');
        const loggedUserPicture = "https://randomuser.me/api/portraits/men/11.jpg";
        const conversation = messages[user].messageList;

        return (
            <View style={styles.container}>
                <FlatList
                    style={{width: '100%'}}
                    contentContainerStyle={styles.conversation}
                    data={conversation}
                    inverted={true}
                    keyExtractor= {(item, id) => `${item.user}-${id}`}
                    renderItem={
                        ({item : message}) => <Bubble
                            user={message.me ? 'Me' : users[user].name}
                            picture={message.me ? loggedUserPicture : users[user].picture}
                            time={message.time}
                            me={message.me}
                            message={message.text}
                        />
                    }
                />
                <View style={styles.inputContainer}>
                    <Entypo name="attachment" size={25} color='grey'/>
                    <TextInput
                        style={styles.input}
                    />
                    <TouchableOpacity style={{minWidth: 40}}>
                        <Text style = {styles.button}>
                            Send
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

Chat.navigationOptions = {
    headerBackImage: <Ionicons name="ios-arrow-back" size={35} />,
    headerStyle: {
        backgroundColor: 'white',
    },
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    conversation: {
        flexDirection: 'column-reverse',
        justifyContent: 'flex-end',
    },
    inputContainer: {
        flexDirection: 'row',
        width: '100%',
        height: 50,
        alignItems: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 4,
        flex: 1,
        marginLeft: '3%',
        marginRight: '2%',
    },
    button: {
        fontWeight: '700',
        fontSize: 12,
    }
});
