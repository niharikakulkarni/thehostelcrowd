import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { newNotifications } from '../../data/notifications';
import users from '../../data/users';

export default class NewNotification extends React.Component {
    render() {
        const notification = newNotifications.map((notif) => {
            const user = users[notif.user];
            return <View key={notif.id} style={notifStyle.container}>
                <TouchableOpacity style={notifStyle.close} aria-label="Clear Notification"><Text style={{fontSize: 35, color: 'grey'}}>&times;</Text></TouchableOpacity>
                    <View style={notifStyle.innerContainer}>
                        <Image source={{uri: user.picture}} style={notifStyle.image}/>
                        <View style={{width: '100%'}}>
                            <Text style={notifStyle.name}>{`${user.name} ${user.lastname}`}</Text>
                            <Text style={notifStyle.date}>{`BHAI - ${notif.date}`}</Text>
                        </View>
                    </View>
                        <Text style={notifStyle.infoBold}>{notif.type}</Text>
                        <Text style={notifStyle.info}>{notif.preview}</Text>
                </View>
        });

        return (
            <View style={styles.container}>
                <Text style={styles.header}>New</Text>
                {notification}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 15,
    },
    header: {
        color: 'white',
        alignSelf: 'flex-start',
        marginBottom: '2%',
        paddingLeft: '5%',
    }
});

const notifStyle = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: '90%',
        height: 150,
        borderRadius: 7,
        paddingLeft: '3%',
        paddingRight: '3%',
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: 10,
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginRight: 5,
    },
    name: {
        fontWeight: '500',
    },
    date: {
        fontWeight: '300',
        fontSize: 12,
    },
    close: {
        position: 'absolute',
        right: 7,
        top: -5,
    },
    infoBold: {
        fontWeight: 'bold',
        width: '100%',
    },
    info: {
        fontWeight: '200',
        fontSize: 12,
        fontStyle: 'italic',
    }
});