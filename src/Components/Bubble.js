import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

class Bubble extends React.Component {
    render() {
        const {user, picture,  message, time, me = false} = this.props;
        return (
            <View style={[styles.commonContainer, me ? styles.meContainer: styles.container]}>
                <View style={me ? styles.meInfo: styles.info}>
                    <Text style={ me? {marginRight: 5} : {marginLeft: 5}}>{time}</Text>
                    <Text>{user}</Text>
                    <Image source={{uri: picture}} style={ [styles.image, me ? styles.myPicture: styles.picture]}/>
                </View>
                <Text>{message}</Text>
            </View>
        )
    }
}

export default Bubble;

const styles = StyleSheet.create({
    commonContainer: {
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        borderRadius: 4,
        minWidth: 190,
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginBottom: 20,
    },
    container: {
        alignSelf: 'flex-start',
        marginLeft: 20,
    },
    meContainer: {
        alignSelf: 'flex-end',
        marginRight: 20,
    },
    info:{
        flexDirection: 'row-reverse',
        justifyContent: 'flex-end'
    },
    meInfo:{
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    image: {
        height: 30,
        width: 30,
        borderRadius: 15,
        position: 'absolute',
    },
    picture: {
        left: '100%',
    },
    myPicture: {
        right: '-25%',
    }
});