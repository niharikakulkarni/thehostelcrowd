import { LOAD_MESSAGES, LOAD_NOTIFICATIONS, LOAD_USERS } from './actionTypes'

export const loadUsers = (users) => ({
    type: LOAD_USERS,
    users,
});

export const loadNotifications = (notifications) => ({
    type: LOAD_NOTIFICATIONS,
    notifications,
});

export const loadMessages = (messages) => ({
    type: LOAD_MESSAGES,
    messages,
});