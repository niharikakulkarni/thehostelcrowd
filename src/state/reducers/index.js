import { combineReducers } from 'redux';
import loaders from './loaders'

export default combineReducers({
    loaders,
});