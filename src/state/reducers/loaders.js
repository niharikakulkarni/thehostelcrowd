const loaders = (state = {}, action) => {
    switch (action.type) {
        case 'LOAD_USERS':
            return {...state,
                users: action.users,
            };

        case 'LOAD_MESSAGES':
            return {...state,
                messages: action.messages,
            };

        case 'LOAD_NOTIFICATIONS':
            return {...state,
                notifications: action.notifications,
            };

        default:
            return state
    }
};

export default loaders;