import React from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';

export default function Empty() {
    return (
        <ScrollView style={styles.container}>
            <Text>Nothing to see here</Text>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
