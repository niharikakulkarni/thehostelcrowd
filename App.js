import React from 'react';
import { Provider } from 'react-redux';
import store from './src/state/store'
import Navigation from  './src/TabNavigator'

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Navigation />
            </Provider>
        )
    }
}