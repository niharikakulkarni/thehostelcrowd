Getting Started:

1. Clone the project  
`git clone https://niharikakulkarni@bitbucket.org/niharikakulkarni/thehostelcrowd.git`

2. Install Expo CLI  
`npm install -g expo-cli`

3. Install Expo App on Phone  
ios: https://apps.apple.com/us/app/expo-client/id982107779  
android: https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_IN

4. Start the project  
`cd thehostelcrowd`  
`npm install`  
`expo start`

5. Scan the QR code using the app on your phone