const users = {
    leanne: {
        id: 1,
        name: "Leanne",
        lastname: "Graham",
        picture: "https://randomuser.me/api/portraits/women/35.jpg",
    },
    ervin: {
        id: 2,
        name: "Ervin",
        lastname: "Howell",
        picture: "https://randomuser.me/api/portraits/men/22.jpg",
    },
    clementine: {
        id: 3,
        name: "Clementine",
        lastname: "Bauch",
        picture: "https://randomuser.me/api/portraits/women/63.jpg",
    },
    patricia: {
        id: 4,
        name: "Patricia",
        lastname: "Lebsack",
        picture: "https://randomuser.me/api/portraits/women/24.jpg",
    },
    chelsey: {
        id: 5,
        name: "Chelsey",
        lastname: "Dietrich",
        picture: "https://randomuser.me/api/portraits/women/46.jpg",
    },
};

export default users;