export const notifications = [
    {
        id: 1,
        type: "Mentioned you",
        preview: "Ok, fine that's good",
        user: "leanne",
    },
    {
        id: 2,
        type: "Commented on your file",
        preview: "Ok, fine that's good",
        user: "ervin",
    },
    {
        id: 3,
        type: "To do is past your due date",
        preview: "Ok, fine that's good",
        user: "clementine",
    },
    {
        id: 4,
        type: "To do is past your due date",
        preview: "Ok, fine that's good",
        user: "patricia",
    },
    {
        id: 5,
        type: "Mentioned you",
        preview: "Ok, fine that's good",
        user: "chelsey",
    },
];

export const newNotifications = [
    {
        id: 0,
        type: "Mentioned you",
        preview: "Can you tell me what you're planning to do regarding this. Please let us know asap",
        user: "chelsey",
        date: "31-July-2019",
    },
];