const messages = {
    leanne: {
        messageList: [
            {
                text: 'Hello',
                time: '10:32',
            },
            {
                text: 'Hi Leanne',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
        ]
    },
    ervin: {
        messageList: [
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Ervin',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
        ]
    },
    clementine: {
        messageList: [
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Clementine',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
        ]
    },
    patricia: {
        messageList: [
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Patricia',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
        ]
    },
    chelsey: {
        messageList: [
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Chelsey',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Chelsey',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
            {
                text: 'Hello',
                time: '10:32'
            },
            {
                text: 'Hi Chelsey',
                time: '12:32',
                me: true,
            },
            {
                text: 'Did you check this out',
                time: '13:56'
            },
            {
                text: 'yes, its nice',
                time: '14:12',
                me: true,
            },
        ]
    },
};

export default messages;